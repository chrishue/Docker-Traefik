# Traefik

[Traefik][traefik_documentation] stack for docker swarm mode.

Traefik is an open-source edge router. It works with many [providers](https://doc.traefik.io/traefik/providers/overview/), with both Docker (standalone) Engine and Docker Swarm Mode by service discovery with labels.

The full documentation can be found [here][traefik_documentation].

[traefik_documentation]: https://doc.traefik.io/traefik/
[traefikgithub]: https://github.com/containous/traefik/

<!-- TOC -->
- [1. Initial Setup](#1-initial-setup)
  - [1.1. Examples](#11-examples)
  - [1.2. Define ports und entrypoints](#12-define-ports-und-entrypoints)
  - [1.3. Create Overlay Networks](#13-create-overlay-networks)
  - [1.4. Deploy stack](#14-deploy-stack)
- [2. Usage](#2-usage)
  - [2.1. How to define routes, services and so on in other Containers](#21-how-to-define-routes-services-and-so-on-in-other-containers)
<!-- /TOC -->

## 1. Initial Setup

### 1.1. Examples

There are multiple examples. Copy them and rename it without `-example`. Alter them for your application and make sure they keep excluded from git..

### 1.2. Define ports and entrypoints

The defined ports must match the `entryPoints` configured in the static configuration.

Examples:
```yml
# traefik.yml
entryPoints:
  http:
    address: ":80"
  https:
    address: ":443"
  tcpmqtt:
    address: ":1883/tcp"
[...]
```
```yml
# stack.override.yml
services:
  traefik:
    [...]
    ports:
      # unsecure http
      - target: 80 # http entrypoint
        published: 80
        protocol: tcp
        mode: host
      # secure http
      - target: 443 # https entrypoint
        published: 443
        protocol: tcp
        mode: host
      # mqtt over tcp as example
      - target: 1883 # tcpmqtt entrypoint
        published: 1883 
        protocol: tcp
        mode: host
```

### 1.3. Create Overlay Networks

```bash
docker network create \
  --opt encrypted --attachable \
  --driver overlay \
  --subnet=172.16.0.0/24 \
  traefik_proxynet
```

```bash
# network to access only the docker socket 
docker network create \
  --opt encrypted --internal \
  --driver overlay \
  --subnet=172.18.1.0/24 \
  traefik_dockersocket
```
`--internal` Creates an externally isolated overlay network. More about [here](https://docs.docker.com/engine/reference/commandline/network_create/#internal).

### 1.4. Deploy stack

As a good practice, all additions to `docker-compose.yml` should be added as an additional compose file. You can also add your environments by this way.

Examples:
```bash
# simple
docker stack deploy -c docker-compose.yml -c stack.yml
```
```shell
# more compley prod/dev 
docker stack deploy -c docker-compose.yml -c prod.swarm.override.yml
docker stack deploy -c docker-compose.yml -c testing.swarm.override.yml
```

## 2. Usage

### 2.1. How to define routes, services and so on in other Containers

**Common Traefik usage example with container labels**

Three details must be configured:
1. `traefik_proxynet` must be defined as an external network at the highest level.
2. `traefik_proxynet` must be added as a network for the publicly accessible service.
3. The dynamic configurations for Traefik are listed as `labels` below `deploy`.

The complete documentation on Traefik's dynamic configuration using `labels` can be found [here](https://doc.traefik.io/traefik/providers/docker/#routing-configuration-with-labels)

Example:

```yml
networks:
  traefik_proxynet:
    external: true

services:
  webapp:
    image: image
    networks:
      - traefik_proxynet
    ports:
      - 8000:80
    deploy:
      labels:
        - "traefik.enable=true"
        - "traefik.docker.network=traefik_proxynet"
        - "traefik.http.routers.example-app.entrypoints=https"
        - "traefik.http.routers.example-app.rule=Host(`app.example.com`)"
        - "traefik.http.routers.example-app.tls=true" # Optional, when 'certresolver' is specified
        - "traefik.http.routers.example-app.tls.certresolver=letsencrypt"
        - "traefik.http.routers.example-app.middlewares=force-https@file, https-defaults@file, localnetwork-ipwhitelist@file"
        - "traefik.http.services.example-app.loadbalancer.server.port=80"
        - "traefik.http.services.example-app.loadbalancer.server.scheme=http"
```
